
# Running example

This docker launches Grafana instance with demo dashboard presenting 2 Grafana plugins: 

* [https://gitlab.com/rentalker/consensus-visualization-plugin](https://gitlab.com/rentalker/consensus-visualization-plugin)
* [https://gitlab.com/rentalker/topology-visualization-plugin](https://gitlab.com/rentalker/topology-visualization-plugin)

## docker-compose
Compose is a tool for defining and running multi-container Docker applications.

Install git-lfs. For Ubuntu:
```sh
sudo apt-get install -y git-lfs
```

```sh
git clone https://gitlab.com/rentalker/grafana-docker-image.git
cd grafana-docker-image
docker-compose up -d
```

# Accessing the panel
When provisionin of the virtual machine will be completed garafana instance will be available at [http://localhost:3000/dashboards](http://localhost:3000/dashboards). Default username and password are ```admin/admin```.

# Viewing the data

To view animated data enter ```now-<hours_passed_from_from>h``` into the "To" field of grafana. Number of hours passed can be obtained using [this link](https://www.wolframalpha.com/input/?i=now+-+2021-11-09T22%3A51%3A53UTC).

# License
See the [License File](LICENSE).

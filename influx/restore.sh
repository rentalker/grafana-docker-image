#!/bin/bash

echo '  Sleeping for 10 seconds'
sleep 10

echo '  Copying backup to /tmp'
cp /home/backup.tar /tmp
cd /tmp
echo '  Extracting backup'
tar -xf backup.tar
echo '  Uploading data'
influxd restore -portable /tmp/backup
